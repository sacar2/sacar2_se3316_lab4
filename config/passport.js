
var passport          = require('passport')
  , GoogleStrategy    = require('passport-google-oauth').OAuth2Strategy
  , FacebookStrategy  = require('passport-facebook').OAuth2Strategy
  , InstagramStrategy = require('passport-instagram').OAuth2Strategy
  , TumblrStrategy    = require('passport-tumblr').OAuth2Strategy;

  var User = require('../app/models/user');
module.exports = function(passport){


  // Passport session setup.
  //   To support persistent login sessions, Passport needs to
  //   serialize users into and deserialize users out of the session.
  //   storing the user ID when serializing, and finding
  //   the user by ID when deserializing.
  //    complete Google profile is serialized and deserialized.
  passport.serializeUser(function(user, done) {
    done(null, user);
  });
  passport.deserializeUser(function(obj, done) {
    done(null, obj);
  });



    passport.use(new GoogleStrategy({
        clientID: "607523802012-2ib065ie7p5fa3u7d95r342h08tul1sj.apps.googleusercontent.com",
        clientSecret: "jO398jICPgkQfWIFCXvfyVkX",
        callbackURL: "http://127.0.0.1:8080/auth/google/callback"
      },
      function(accessToken, refreshToken, profile, done) {
        User.findOrCreate({ googleId: profile.id }, function (err, user) {
          return done(err, user);
        });
      }
    ));

    // GOOGLE AUTH REDIRECT
        //When complete, Google will redirect the user back to the application at   /auth/google/return

        app.get('/auth/google', passport.authenticate('google', { scope: [
               'https://www.googleapis.com/auth/plus.login',
               'https://www.googleapis.com/auth/plus.profile.emails.read']
        }));

            // Google will redirect the user to this URL after authentication.  Finish
            // the process by verifying the assertion.  If valid, the user will be
            // logged in.  Otherwise, authentication has failed.
            app.get('/auth/google/callback', passport.authenticate('google',{
                  failureRedirect: '/login' }),
                  function(req, res) {
                    //if this use has signed in before

                    //if this is a new user


                  res.redirect('/dash');
            });

}
