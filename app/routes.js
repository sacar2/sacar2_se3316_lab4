

//tell the page that express will be associated with writing the views on a single page
// get an instance of the express Router
  var router = express.Router();

// middleware for all requests - done everytime a request is sent
  router.use(function(req, res, next) {
      // do logging
      console.log('Something is happening.');
      next(); // make sure we go to the next routes and don't stop here
  });


// test the route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/', function(req, res) {
      res.render('login.ejs', { user: req.user });
  });

// test the route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/login', function(req, res) {
      res.render('login.ejs', { user: req.user });
  });

// test the route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/dashboard', function(req, res) {
      res.render('dash.ejs', { user: req.user });
  });

// test the route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/logout', function(req, res) {
      res.render('login.ejs', { user: req.user });
  });
