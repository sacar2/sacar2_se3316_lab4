var mongoose =  require('mongoose');
var Schema       = mongoose.Schema;

var userSchema = new Schema({

    local            : {
        email        : String,
        password     : String,
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    pinterest        : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    instagram        : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }

});



// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
