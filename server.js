
// BASE SET UP
// pull in all the packages needed
// ======================================================================

var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var port      = process.env.PORT ||8080;      //set the port
var mongoose  = require('mongoose');
var db = mongoose.connection;
var flash   = require('connect-flash');
var Bear    = require('./app/models/bear');
var User    = require('./app/models/user');


//CONFIGURE
//========================================================================

app.use(bodyParser.urlencoded({ extended: true}));          //configure app to use bodyParser()
app.use(bodyParser.json());                                 //use data from POST
app.set('view engine', 'ejs');                              // set up ejs for templating
mongoose.connect('mongodb://localhost:27017/gibberishDB');  // connect to database
db.on('error',console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log("Connected to db");
});

var passport          = require('passport')
  , GoogleStrategy    = require('passport-google-oauth').OAuth2Strategy
  , FacebookStrategy  = require('passport-facebook').OAuth2Strategy
  , InstagramStrategy = require('passport-instagram').OAuth2Strategy
  , TumblrStrategy    = require('passport-tumblr').OAuth2Strategy;


require('./config/passport')(passport); // configure passport for oauth



  // passport.use('passport-facebook', new FacebookStrategy({
  //     clientID: "762292237250349",
  //     clientSecret: "075c62da53ecd6a06fa68e37aad41d96",
  //     callbackURL: "http://localhost:8080/auth/facebook/callback",
  //     enableProof: false
  //   },
  //   function(accessToken, refreshToken, profile, done) {
  //     User.findOrCreate({ facebookId: profile.id }, function (err, user) {
  //       return done(err, user);
  //     });
  //   }
  // ));

//FACEBOOK AUTH REDIRECT
    // app.get('/connect/facebook',
    //   passport.authorize('facebook'));
    //
    // app.get('/connect/facebook/callback',
    //   passport.authorize('facebook-authz', { failureRedirect: '/login' }),
    //   function(req, res) {
    //     // Successful authentication, redirect home.
    //     res.redirect('/dash');
    //     onsole.log('logged into fb!');
    // });


// ROUTES FOR API
// =============================================================================

require('./app/routes.js')(app, passport); //all the routes








//ACT ON ROUTE on routes that end in /bears
// =============================================================================
router.route('/bears')
//handle multiple routes for same URI
 //anything that ends in '/bears'

    // POST ===== create a bear (accessed at POST http://localhost:8080/api/bears)
    .post(function(req, res) {

        var bear = new Bear();      // create a new instance of the Bear model
        bear.name = req.body.name;  // set the bears name (comes from the request)

        // save the bear and check for errors
        bear.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear created!' });
        });
      })

    // GET ===== get ALL the bears (accessed at GET http://localhost:8080/api/bears)
    .get(function(req, res) {
        Bear.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });

    });

//ROUTES FOR A SINGLE ITEM (get an attr, put an attr, delete bear)
//=============================================================================
//route to handle requests that have a bearID associated
router.route('/bears/:bear_id')

    // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
    .get(function(req, res) {
        Bear.findById(req.params.bear_id, function(err, bear) {
            if (err)
                res.send(err);
            res.json(bear);
        });
    })

    // update the bear with this id (accessed at PUT http://localhost:8080/api/bears/:bear_id)
.put(function(req, res) {

    // use our bear model to find the bear we want
    Bear.findById(req.params.bear_id, function(err, bear) {

        if (err)
            res.send(err);

        bear.name = req.body.name;  // update the bears info

        // save the bear
        bear.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear updated!' });
        });

    });
})

.delete(function(req, res) {
        Bear.remove({
            _id: req.params.bear_id
        }, function(err, bear) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
});

// REGISTER ROUTES -------------------------------
app.use('/', router);    // routes will be prefixed with '/api'
app.use( express.static( "public" ) );

// START THE SERVER
// =============================================================================

app.listen(port);   //express app listens to the port
console.log('Magic happens on port ' + port);   //test the port

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login');
}
